# Используем базовый образ с поддержкой нужного языка и пакетов
FROM nginx:latest

# Информация о создателе образа
MAINTAINER Мамасолиев Д.А.

# Обновление списка пакетов и установка необходимых
RUN apt-get update && \
    apt-get install -y postgresql && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Настройка рабочей директории
WORKDIR /var/www/html

# Установка переменных среды для PostgreSQL
ENV POSTGRES_USER=admin \
    POSTGRES_PASSWORD=admin

# Добавление файлов и директорий
COPY index.html /var/www/html/index.html
ADD nginx.conf /etc/nginx/nginx.conf

# Создание тома для данных PostgreSQL
VOLUME /var/lib/postgresql/data

# Открытие портов
EXPOSE 80 5432

# Установка пользователя по умолчанию
USER nginx

# Команда запуска сервера
CMD ["nginx", "-g", "daemon off;"]
